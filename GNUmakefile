# makefile

LOGO    = OGL-Logo.jpeg

build:
	mkdir $@

help:
	@echo "make concat   Concatenates files."
	@echo "make html     Generate book as HTML."
	@echo "make txt      Generate book as plain text."
	@echo "make epub     Generate book as an epub."
	@echo "make pdf      Generate book as an PDF."
	@echo "make clean    Remove temp files."

concat: src/header.xml src/footer.xml build
	@test -d build || mkdir build
	@cat src/header.xml \
	src/chapter.xml \
	src/chapter2.xml \
	src/OGLv1.0a.xml \
	src/cc-by-sa.xml \
	src/colophon.xml \
	src/footer.xml > tmp.xml
	@egrep '\<para\>.*dom' src/chapter2.xml > swap.tmp
	./swapper.sh 

html:	build concat
	@mkdir dist  || true
	xmlto --skip-validation -o build html-nochunks tmp.xml
	@mv build/tmp.html dist/peganad20.html

txt:	build concat
	@mkdir dist  || true
	xmlto --skip-validation -o build txt tmp.xml
	@mv build/tmp.txt dist/peganad20.txt

epub:	build concat
	@mkdir build/epub
	xsltproc --output build/epub/ docbook/epub/docbook.xsl tmp.xml
	#xmlto --skip-validation -o build epub tmp.xml
	#unzip build/tmp.epub -d build/epub
	@cp img/$(LOGO) build/epub/OEBPS/$(LOGO)
	@cp img/cover-front.jpeg build/epub/OEBPS/cover.jpeg
	@cp font/*ttf build/epub/OEBPS/
	@cat style/style.css > build/epub/OEBPS/style.css 
	@sed -i 's_../img/__g' build/epub/OEBPS/index.html
	@sed -i 's_../img/__g' build/epub/OEBPS/content.opf
	@sed -i 's_jpeg" media-type=""/>_jpeg" media-type="image/jpeg"/>_g' build/epub/OEBPS/content.opf
	@sed -i 's_ncx"/>_ncx"/><item id="idm0" href="Andada-Regular.ttf" media-type="application/x-font-ttf"/><item id="idm2" href="cover.jpeg" media-type="image/jpeg"/><item id="idm3" href="junction-bold.ttf" media-type="application/x-font-ttf"/><item id="idm4" href="junction-light.ttf" media-type="application/x-font-ttf"/><item id="idm5" href="junction-regular.ttf" media-type="application/x-font-ttf"/><item id="idm6" href="style.css" media-type="text/css"/><item id="idm7" href="texgyrebonum-bold.ttf" media-type="application/x-font-ttf"/><item id="idm8" href="texgyrebonum-bolditalic.ttf" media-type="application/x-font-ttf"/><item id="idm9" href="texgyrebonum-italic.ttf" media-type="application/x-font-ttf"/><item id="idm10" href="texgyrebonum-regular.ttf" media-type="application/x-font-ttf"/>_' build/epub/OEBPS/content.opf
	@find build/epub/OEBPS/ -name "*html" -exec sed -i 's_<head>_<head>\n\n_' {} \;
	#@find build/epub/OEBPS/ -name "*html" -exec sed -i "/<head>/r style/style.css" {} \;
	@find build/epub/OEBPS/ -name "*html" -exec sed -i 's_</head>_<link rel="stylesheet" href="style.css" /></head>_' {} \;
	@mv build/epub/OEBPS .
	@mv build/epub/META-INF .
	@cat src/mimetype > mimetype
	@zip -X -0 peganad20.epub mimetype
	@rm mimetype
	@zip -X -9 peganad20.epub -r META-INF OEBPS
	@rm -rf META-INF OEBPS
	@mv peganad20.epub dist

pdf:	build concat
	@mkdir build || true
	@mkdir dist  || true
	xsltproc --output build/tmp.fo \
	 --stringparam paper.type  A4 \
	 --stringparam page.width 8.25in \
	 --stringparam page.height 10in \
	 --stringparam my.guild.logo "../img/redline.svg" \
	 --stringparam redist.text "sa" \
	 --stringparam column.count.titlepage 1 \
	 --stringparam column.count.lot 1 \
	 --stringparam column.count.front 1 \
	 --stringparam column.count.body 2 \
	 --stringparam column.count.back 1 \
	 --stringparam column.count.index 2 \
	 --stringparam body.font.family "TeX Gyre Bonum" \
	 --stringparam title.font.family "Andada" \
	 --stringparam bridgehead.font.family "Junction" \
	 --stringparam symbol.font.family "UniCons" \
	 --stringparam footer.column.widths "1 0 0" \
	 --stringparam body.font.master 10 \
	 --stringparam body.font.size 10 \
	 --stringparam page.margin.inner .25in \
	 --stringparam page.margin.outer .5in \
	 --stringparam page.margin.top .45in \
	 --stringparam page.margin.bottom .45in \
	 --stringparam title.margin.left 0 \
	 --stringparam title.start.indent 0 \
	 --stringparam body.start.indent 0 \
	 --stringparam chapter.autolabel 0 \
	style/mystyle.xsl tmp.xml
	fop -c style/rego.xml build/tmp.fo build/tmp.pdf
	convert img/cover-front.jpeg build/cover.pdf
	pdf-stapler cat build/cover.pdf build/tmp.pdf dist/peganad20.pdf || mv build/tmp.pdf dist/peganad20.pdf

clean:	build
	@rm -rf build
	@rm -f tmp*xml
	@rm -f *.tmp
	@rm -rf OEBPS
