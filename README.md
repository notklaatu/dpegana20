# Pegana for RPG

The Pegana RPG adapted for d20 RPG systems.


## Using these stylesheets

You don't need to build this from source. You can download it already built as a PDF or epub.

If you are adapting this work, though, you might need to build from source.

This has only been tested on Linux and BSD, but as long as you know Docbook, you should be able to use this with only modest changes to the GNUmakefile.
      
*Assuming you're using Linux or BSD, the required software is probably available from your software repository or ports tree.*

Requirements:

* [Docbook](http://docbook.org)
* [Junction](https://www.theleagueofmoveabletype.com/junction) font
* [Andada](http://www.1001fonts.com/andada-font.html)
* [TeX Gyre Bonum](http://www.1001fonts.com/tex-gyre-bonum-font.html) (bundled in this repo to fix render errors)
* [xmlto](https://pagure.io/xmlto)
* [xsltproc](http://xmlsoft.org/XSLT/index.html)
* [FOP 2](https://xmlgraphics.apache.org/fop/)
* [pdftk](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/)
* [Image Magick](http://imagemagick.org/script/index.php)

Optional, to avoid warnings about not having a **symbol** font installed:

* [UniCons](https://fontlibrary.org/en/font/unicons)


### Setup

The font **TeX Gyre Bonum** does not function properly because it's OTF rather than TTF.

I have converted the source OTF to TTF for you. They're included with this repository.

Place them in your ``$HOME/.local/share/fonts/t/`` directory so Docbook can detect them.


### GNUmakefile

The GNUmakefile builds a PDF, the standard delivery format for most indie RPG adventures. You can edit inline parameters to suit your needs.

Notably:

* paper size is A4 by default
* license is set to ``no`` by default, which renders the standard *for personal use only* footer. You can alternately set it to ``sa`` for a Creative Commons BY-SA license message in the footer, instead. The Open Game License only covers mechanics, so your CC license only applies to your *story content*
* the name of the output file is ``example.pdf`` by default

Once you've written your adventure and are ready to build:

    $ make clean pdf
    
The output is placed into a directory called ``dist``.


## Bugs

You don't have to be a professional designer to detect that this isn't an exact match for the DMs Guild templates. This is but a [mimic](http://paizo.com/pathfinderRPG/prd/bestiary/mimic.html#mimic). When I retire, I may revisit this and try to style it to be indistinguishable from the official templates, but to the average reader, this is close enough.

Report any other bugs you find, or even feedback or suggestions, in the issues tab.

## Contribute

Yes, you can contribute. I do have some ugly hacks in here (for instance, I'm re-purposing ``informalexample`` for the singular purpsoe of centering the dmsguild logo on the title page), so if you know XSL or XML, please make improvements and send them my way. I'm happy to incorporate improvements.

## License

It's all open source and free culture. See LICENSE file for details.