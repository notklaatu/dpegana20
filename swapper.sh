#!/bin/sh

echo "Preprocessing text..."
n=1
t=`wc -l swap.tmp | cut -f1 -d" "`
while [[ $n -lt $t ]] ; do
sed -n "${n}"p swap.tmp > LINE.tmp
NEW=`sed -n "${n}"p swap.tmp`
SWAP=`echo $NEW | awk -F'"' '{ print $4 }'`
echo $SWAP
let n++
sed -i "/SCRIPTENT:$SWAP/c $NEW" tmp.xml ;
done
